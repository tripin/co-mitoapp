/**
 * Created by Eran on 18/03/2016.
 */
var config = require('./config'),
    mongoose = require('mongoose');

module.exports = function() {
    var db = mongoose.connect(config.db);
    require('../app/models/user.server.model');
    require('../app/models/event.server.model');
    require('../app/models/segment.server.model');
    return db;
};