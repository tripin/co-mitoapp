/**
 * Created by Eran on 18/03/2016.
 */
var config = require('./config'),
    express = require('express'),
    bodyParser = require('body-parser'),
    passport = require('passport'),
    flash = require('connect-flash'),
    session = require('express-session'),
    path = require('path'),
    favicon = require('serve-favicon');

module.exports = function() {
    var app = express();

    app.use(bodyParser.urlencoded({
        extended: true
    }));

    app.use(bodyParser.json());

    app.use(session({
        saveUninitialized: true,
        resave: true,
        secret: 'OurSuperSecretCookieSecret'
    }));




    app.set('views', './app/views');
    app.set('view engine', 'ejs');
    app.use(express.static(path.join(__dirname + '/../', 'public')));
    console.log('public path: '+__dirname + 'public')
    app.use(favicon(__dirname+'/../public/favicon.ico'));

    app.use(flash());
    app.use(passport.initialize());
    app.use(passport.session());

    require('../app/routes/index.server.routes.js')(app);
    require('../app/routes/users.server.routes.js')(app);
    require('../app/routes/event.server.routes.js')(app);
    require('../app/routes/segment.server.routes.js')(app);

    app.use(express.static('./public'));

    return app;
};