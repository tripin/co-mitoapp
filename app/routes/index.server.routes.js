/**
 * Created by Eran on 19/03/2016.
 */
module.exports = function(app) {
    var index = require('../controllers/index.server.controller');
    app.get('/', index.renderHome);
};