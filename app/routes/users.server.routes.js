/**
 * Created by Eran on 18/03/2016.
 */
var users = require('../../app/controllers/users.server.controller'),
    passport = require('passport');

module.exports = function(app) {
    app.route('/users')
        .post(users.create)
        .get(users.isLoggedIn, users.list);

    //app.use(users.isLoggedIn);

    app.route('/users/:userId')
        .get(users.read)
        .post(users.update);

    app.route('/del-users/:userId')
        .post(users.delete);

    app.param('userId', users.userByID);

    app.get('/logout', users.logout);

    app.get('/oauth/facebook', passport.authenticate('facebook', {
        failureRedirect: '/login',
        scope:['email', 'public_profile']
    }));

    app.get('/oauth/facebook/callback', passport.authenticate('facebook', {
        failureRedirect: '/login',
        successRedirect: '/',
        scope:['email', 'public_profile']
    }));

    app.get('/is-authenticated/', function(req, res) {
        console.log(req.isAuthenticated())
        res.json({isAuthenticated: req.isAuthenticated()})
    })

};