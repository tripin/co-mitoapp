/**
 * Created by Eran on 22/03/2016.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var segmentSchema = new Schema({
    name: String
});

mongoose.model('Segment', segmentSchema);

