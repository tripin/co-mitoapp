/**
 * Created by Eran on 18/03/2016.
 */

//scope.userData={img:"" , userName: "or" , userLastName:"enjoy" ,email:"",location:"Tel aviv",age:"23" ,facebookInformation:{friends:"" ,likes:""}};
//$scope.userIntrest=[ {title:"music" ,data:[{description:"beyonce" , id:"1"}]},
//    {title:"hobbies" ,data:[{description:"sport" , id:"1"},{description:"fashion" , id:"2"}]},
//    {title:"books" ,data:[{description:"aa" , id:"1"}]},
//];



var mongoose = require('mongoose'),
    crypto = require('crypto'),
    Schema = mongoose.Schema;

var UserSchema = new Schema({
    name: String,
    email: String,
    username:{
        type:String,
        unique: true
    },
    events: [{type: Schema.Types.ObjectId, ref: 'Event'}],
    reviews: [{type: Schema.Types.ObjectId, ref: 'Review'}],
    password: String,
    provider: String,
    providerId: String,
    providerData: {}
});

UserSchema.pre('save',
    function(next) {
        if (this.password) {
            var md5 = crypto.createHash('md5');
            this.password = md5.update(this.password).digest('hex');
        }

        next();
    }
);

UserSchema.methods.authenticate = function(password) {
    var md5 = crypto.createHash('md5');
    md5 = md5.update(password).digest('hex');

    return this.password === md5;
};

UserSchema.statics.findUniqueUsername = function(username, suffix, callback) {
    var _this = this;
    var possibleUsername = username + (suffix || '');

    _this.findOne(
        {username: possibleUsername},
        function(err, user) {
            if (!err) {
                if (!user) {
                    callback(possibleUsername);
                }
                else {
                    return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
                }
            }
            else {
                callback(null);
            }
        }
    );
};
mongoose.model('User', UserSchema);