///**
// * Created by Eran on 22/03/2016.
// */
//$scope.event.startDate= $scope.startDate;
//$scope.event.endDate= $scope.endDate;
//$scope.event.startHour= $scope.startHour;
//$scope.event.endHour= $scope.endHour;
//$scope.event.location= $scope.location;
//$scope.event.peopleCapacity= $scope.peopleCapacity;
//$scope.event.title= $scope.title;
//$scope.event.description= $scope.description;

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var EventSchema = new Schema({

    startDate: { type: Date, default: Date.now }, //Wed Mar 23 2016 14:21:08 GMT+0200 (Jerusalem Standard Time)
    endDate: { type: Date, default: Date.now},
    startHour: String,
    endHour: String,
    location: String,
    peopleCapacity: Number,
    title: String,
    Description: String,
    attending:{type:Number, default:0},
    createdBy: {type: Schema.Types.ObjectId, ref: 'User'}

});


mongoose.model('Event', EventSchema);