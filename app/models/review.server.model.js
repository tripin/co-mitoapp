/**
 * Created by Eran on 22/03/2016.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var review = new Schema({
    text: String,
    creation_date: { type: Date, default: Date.now },
    createdBy: {type: Schema.Types.ObjectId, ref: 'User'},
    event:{type: Schema.Types.ObjectId, ref:'Events'}
}, {strict: true})

mongoose.model('Review', review);

