/**
 * Created by Eran on 22/03/2016.
 */
var Segment = require('mongoose').model('Segment')



exports.eventByID = function(req, res, next, id) {
    Event.findOne({_id: id}, function(err, segment) {
            if (err) {
                console.log('No event ID found')
                return next(err);
            }
            else {
                console.log('ID found')
                req.segment = segment;
                next();
            }
        }
    );
};

exports.create = function(req, res, next) {
    var segment = new Segment(req.body);
    segment.save(function(err) {
        if (err) {
            return next(err);
        }
        else {
            res.json(segment);
        }
    });
};

exports.list = function(req, res, next) {
    Segment.find({}, function (err, segments) {
        if (err) {
            return next(err);
        }
        else {
            res.json(segments);
        }
    });
};

exports.update = function(req, res, next) {
    Event.findByIdAndUpdate(req.event._id, req.body, {new: true},function(err, segment) {
        if (err) {
            return next(err);
        }
        else {
            res.json(segment);
        }
    });
};

exports.delete = function(req, res, next) {
    req.segment.remove(function(err) {
        if (err) {
            return next(err);
        }
        else {
            res.json(req.segment);
        }
    })
};

exports.read = function(req, res) {

    res.json(req.segment);
};

