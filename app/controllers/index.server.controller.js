/**
 * Created by Eran on 19/03/2016.
 */
var path = require('path');

exports.renderHome =  function(req, res) {
    res.sendFile(path.join(__dirname, '../views', 'index.html'));
};