/**
 * Created by Eran on 22/03/2016.
 */

var Event = require('mongoose').model('Event')



exports.eventByID = function(req, res, next, id) {
    Event.findOne({_id: id}, function(err, event) {
            if (err) {
                console.log('No event ID found')
                return next(err);
            }
            else {
                console.log('Event ID found')
                req.event = event;
                next();
            }
        }
    );
};

exports.create = function(req, res, next) {
    console.log("Saving event");
    var event = new Event(req.body);
    event.save(function(err) {
        if (err) {
            return next(err);
        }
        else {
            res.json(event);
        }
    });
};

exports.list = function(req, res, next) {
    Event.find({}, function (err, events) {
        if (err) {
            return next(err);
        }
        else {
            res.json(events);
        }
    });
};

exports.update = function(req, res, next) {
    Event.findByIdAndUpdate(req.event._id, req.body, {new: true},function(err, event) {
        if (err) {
            return next(err);
        }
        else {
            res.json(event);
        }
    });
};

exports.delete = function(req, res, next) {
    req.event.remove(function(err) {
        if (err) {
            return next(err);
        }
        else {
            res.json(req.event);
        }
    })
};

exports.read = function(req, res) {

    res.json(req.event);
};

