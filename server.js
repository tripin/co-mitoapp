/**
 * Created by Eran on 18/03/2016.
 */
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var config = require('./config/config'),
    mongoose = require('./config/mongoose'),
    express = require('./config/express'),
    passport = require('./config/passport');


var db = mongoose(),
    app = express(),
    passport = passport();

app.listen(config.port);

module.exports = app;
console.log(process.env.NODE_ENV  + ' server running at http://localhost:' + config.port);
console.log("I Wanted to check if it ran");