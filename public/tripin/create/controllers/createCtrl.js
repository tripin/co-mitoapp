'use strict';
myApp.controller('CreateCtrl', ['$scope', function($scope,serverConnection) {

    $scope.init=function(){
        //$scope.event=serverConnection.newEvent()
    }

    $scope.submit=function(){
        //send to Db
        $scope.event.startDate= $scope.startDate;
        $scope.event.endDate= $scope.endDate;
        $scope.event.startHour= $scope.startHour;
        $scope.event.endHour= $scope.endHour;
        $scope.event.location= $scope.location;
        $scope.event.peopleCapacity= $scope.peopleCapacity;
        $scope.event.title= $scope.title;
        $scope.event.description= $scope.description;
        $scope.event.$save().then(function(){
            // to do handler
        })

    };
    $scope.startDate;
    $scope.endDate;
    $scope.startHour;
    $scope.endHour;
    $scope.location;
    $scope.peopleCapacity;
    $scope.title;
    $scope.description;
    $scope.hourOptions=[
        "12:00","13:00","14:00","15:00","16:00","19:00","20:00",
        "21:00","22:00","23:00","24:00","01:00","02:00",
        "03:00","04:00","05:00","06,00","07:00",
        "08:00","09:00","10:00","11:00"

    ]
    $scope.peopleCapacityOptions=["limit to 5" ,"limit to 10","limit to 20","limit to 30","limit to 100" ,"limit to 500" ,"no limit"]
}]);