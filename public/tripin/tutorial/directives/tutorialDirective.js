'use strict';

myApp.directive('tutorial', function() {
    return {
        restrict: 'E',
        scope: {
            config:"="
        },
        templateUrl: 'tripin/tutorial/views/tutorial.html',
        link: function ($scope, element, attributes, ctrl) {
        }
    };
});