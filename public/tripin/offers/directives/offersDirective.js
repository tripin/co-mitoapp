'use strict';

myApp.directive('offers', function() {
    return {
        restrict: 'E',
        scope: {
            config:"="

        },
        templateUrl: 'tripin/offers/views/offers.html',
        link: function ($scope, element, attributes, ctrl) {
        }
    };
});