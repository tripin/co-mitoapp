
/**
 * Created by or enjoy on 04/08/2015.
 */
angular.module('tripin').controller('mainCtrl', function($scope ,$rootScope) {
    $rootScope.components={
        getStarted:false,
        welcome:true,
        calendar:false,
        profile:false,
        login:false,
        offers:false,
        create:false,
        segmentation:false,
        tutorial:false};
    $rootScope.appData={
        homeTown:"",
        visitStartDate:"",
        visitEndDate:"",
        appCity:"Berlin"
    }
    $scope.showSideBar=false;
    $scope.citiesOptions=["Berlin" ,"London", "Paris" , "Tel Aviv"];


});