 angular
        .module('app.services',[])
        .factory('serverConnection', ['$http', function ($http) {

            var service = {

                getEvents: getEvents,
                getEvent: getEvent,
                deleteEvent: deleteEvent,
                updateEvent: updateEvent,
                createEvent: createEvent,
                getSegmentations: getSegmentations,
                deleteSegmentations: deleteSegmentations,
                getUsers: getUsers,
                getUser: getUser,
                createUser: createUser,
                updateUser: updateUser,
                deleteUser: deleteUser
            };

            return service;


            function getEvents(callback) {
                $http.get('/events').success(function (events) {
                    callback(events)
                }).error(function () {
                    //something
                })
            }

            function getEvent(EventId, callback) {
                $http.get('/events/' + EventId).success(function (event) {
                    callback(event)
                }).error(function () {
                    //something
                });
            }

            function deleteEvent(EventId, callback) {
                $http.post('/del-events/' + EventId).success(function (event) {
                    callback(event)
                }).error(function () {
                    //something
                });
            }

            function updateEvent(EventId, body, callback) {
                $http.post('/events/' + EventId, body).success(function (event) {
                    callback(event)
                }).error(function () {
                    //something
                });
            }

            function createEvent(body, callback) {
                $http.post('/events', body).success(function (event) {
                    callback(event)
                }).error(function () {
                    //something
                });
            }

            function getUsers(callback) {
                $http.get('/users').success(function (users) {
                    callback(users)
                }).error(function () {
                    //something
                })
            }

            function getUser(userId, callback) {
                $http.get('/users/' + userId).success(function (user) {
                    callback(user)
                }).error(function () {
                    //something
                })
            }

            function createUser(body, callback) {
                $http.post('/users', body).success(function (user) {
                    callback(user)
                }).error(function () {
                    //something
                });
            }

            function updateUser(userId, body, callback) {
                $http.post('/users/' + userId, body).success(function (user) {
                    callback(user)
                }).error(function () {
                    //something
                });
            }

            function deleteUser(userId, callback) {
                $http.post('/users/' + userId).success(function (user) {
                    callback(user)
                }).error(function () {
                    //something
                });
            }

            function getSegmentations(callback) {
                $http.get('/segments').success(function (segments) {
                    callback(segments)
                }).error(function () {
                    //something
                })
            }

            function deleteSegmentations(segId, callback) {
                $http.get('/del-segments/' + segId).success(function (segment) {
                    callback(segment)
                }).error(function () {
                    //something
                })
            }

        }])


