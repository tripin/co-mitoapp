'use strict';
myApp.controller('SegmentationCtrl', ['$scope','serverConnection','$rootScope', function($scope,serverConnection,$rootScope) {


    $scope.init=function(){
        // get data from DB
        serverConnection.getSegmentations(function(segments) {
            $scope.segmentTypes = segments;

        })
};

    $scope.style={1:0 ,2:0,3:0,4:0,5:0,6:0,7:0,8:0}
    $scope.counter=0;
    $scope.userSegment=[]
    //$scope.segmentTypes=[{name:"sport" , id:1},{name:"sport" , id:2},{name:"sport" , id:3},{name:"sport" , id:4}
    //,{name:"fashion" , id:5},{name:"beer" , id:6},{name:"shopping" , id:7},{name:"food" , id:8}]
    $scope.addSegment= function(id){
        $scope.userSegment.push(id);
        $scope.counter++;
        $scope.style[id]=1;
        if($scope.counter==3){
            $rootScope.components.login=true;
            $rootScope.components.segmentation=false;
        }

    }
    $scope.doneClicked=false;
}]);