'use strict';

myApp.directive('segmentation', function() {
    return {
        restrict: 'E',
        scope: {
            config:"="
        },
        templateUrl: 'tripin/segmentation/views/segmentation.html',
        link: function ($scope, element, attributes, ctrl) {
        }
    };
});