'use strict';

myApp.directive('welcome', function() {
    return {
        restrict: 'E',
        scope: {
            config:"="
        },
        templateUrl: 'tripin/welcome/views/welcome.html',
        link: function ($scope, element, attributes, ctrl) {
        }
    };
});