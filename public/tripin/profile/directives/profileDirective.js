'use strict';


myApp.directive('profile', function() {
    return {
        restrict: 'E',
        scope: {
            config:"="

        },
        templateUrl: 'tripin/profile/views/profile.html',
        link: function ($scope, element, attributes, ctrl) {
        }
    };
});